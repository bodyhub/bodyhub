# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table produkt (
  artikelnummer             integer auto_increment not null,
  name                      varchar(255),
  preis                     float,
  anzahl                    integer,
  beschreibung              varchar(255),
  kategorie                 varchar(255),
  bildurl                   varchar(255),
  constraint pk_produkt primary key (artikelnummer))
;

create table trainingsplan (
  id                        integer auto_increment not null,
  user                      varchar(255),
  titel                     varchar(255),
  uebung                    varchar(255),
  wiederholungen            varchar(255),
  saetze                    varchar(255),
  constraint pk_trainingsplan primary key (id))
;

create table users (
  email                     varchar(255) not null,
  username                  varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  password                  varchar(255),
  constraint uq_users_username unique (username),
  constraint pk_users primary key (email))
;

create table warenkorb (
  warenkorbid               integer auto_increment not null,
  user                      varchar(255),
  artikelnummer             integer,
  constraint pk_warenkorb primary key (warenkorbid))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table produkt;

drop table trainingsplan;

drop table users;

drop table warenkorb;

SET FOREIGN_KEY_CHECKS=1;

