package controllers;

import it.innove.play.pdf.PdfGenerator;
import models.Produkt;
import models.Trainingsplan;
import models.Users;
import models.Warenkorb;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import play.data.*;
import java.util.*;
import com.avaje.ebean.*;
import static play.data.Form.form;


public class Application extends Controller {


    public static Form<Users> user_form = form(Users.class);
    public static Form<Trainingsplan> tp_form = form(Trainingsplan.class);
    public static Model.Finder<String, Trainingsplan> find = new Model.Finder(String.class, Trainingsplan.class);


    public static Result userTp(String benutzer){
        List<Trainingsplan> t = Trainingsplan.findByUser(benutzer);
        return ok(tpfertig.render(t));
    }


    public static Result search(String text) {

        System.out.print("Searching...");

        List<Trainingsplan> t = find.where().eq("titel", text).findList();

        List<Trainingsplan> b  = find.where().istartsWith("titel", text).findList();

        SqlQuery query = Ebean.createSqlQuery("select distinct titel, user from trainingsplan where titel like '"+text+"%';");
        List <SqlRow> c = query.findList();

        return ok(views.html.searchlist.render(c));

    }

    //register dat user
    public static Result register() {

        if (request().method() == "POST") {

            Form<Users> filledForm = user_form.bindFromRequest();
            if (filledForm.hasErrors()) {
                redirect("/register");
            } else {


                //save into database
                Users newUser = new Users();
                newUser.email = filledForm.get().email;
                newUser.firstName = filledForm.get().firstName;
                newUser.lastName = filledForm.get().lastName;
                newUser.username = filledForm.get().username;
                newUser.password = filledForm.get().password;

                //ask if email/username already in database
                if (newUser.findByEmail(newUser.email) != null) {

                    flash("emailError", "Email ist bereits registriert. Loge dich ein...");
                    return redirect("/register");
                } else if (newUser.findByUsername(newUser.username) != null) {

                    flash("usernameError", "Username bereits vergeben");
                    return redirect("/register");
                } else {
                    newUser.save();
                    flash("afterRegister", "Registrierung erfolgreich. Melde dich an!");
                    return redirect("/login");

                }

            }
        }
        flash("wrongRegister", "Error");
        return redirect("/register");
    }

    //login dat user
    public static Result authenticate() {


        if (request().method() == "POST") {

            Form<Users> filledForm = user_form.bindFromRequest();
            Users user = new Users();


            //Ask database if user exists
            Login existUser = new Login();
            if (filledForm.hasErrors()) {
                redirect("/login");
            } else {


                existUser.email = filledForm.get().email;
                existUser.password = filledForm.get().password;

                if (user.findUser(existUser.email, existUser.password) != null) {
                    session().clear();
                    session("email", filledForm.get().email);

                    return redirect("/home");


                }


            }
        }
        flash("wrongLogin", "Falsches Email oder Password");
        return redirect("/login");
    }


    //NO PAIN NO GAIN
    public static Result speicherTrainingsplan() {

        DynamicForm data = form().bindFromRequest();


        if (session().get("email") == null) {
            flash("trainingError", "Du musst eingelogt sein um Trainingsplan zu erstellen");
            return redirect("/trainingsplan");
        } else {

            String email = session().get("email");

            int anzahlUbungen = (data.get().getData().size() / 3);

            for (int i = 0; i < anzahlUbungen; i++) {

                Ebean.save(new Trainingsplan(email, null, data.get("ubung" + i), data.get("set" + i), data.get("rep" + i)));

            }
        }
        return redirect("/tpfertig");
    }

    public static Result shoper(String text) {


        List<Produkt> p1 = Produkt.find.where().eq("kategorie", text).findList();



        return ok(shop.render(p1));
    }



    public static Result leseTrainingsplan() {

        List<Trainingsplan> t = Trainingsplan.findByUser(session().get("email"));
        return ok(tpfertig.render(t));
    }


    public static Result loescheTrainingsplan() {

        Trainingsplan.deleteTrainingsplan(session().get("email"));

        return redirect("/tpfertig");
    }

    public static Result druckeTrainingsplan() {
        List<Trainingsplan> t = Trainingsplan.findByUser(session().get("email"));

        return PdfGenerator.ok(tpfertig.render(t), "http://localhost:9000/pdf");
    }

    public static Result loescheZeile() {
        DynamicForm df = form().bindFromRequest();
        String ubung = df.get("tab-ubung");
        String saetze = df.get("tab-saetze");
        String wiederholungen = df.get("tab-wiederholungen");
        Trainingsplan.deleteUbung(session().get("email"), ubung, saetze, wiederholungen);
        return redirect("/tpfertig");
    }

    public static Result trainingsplanName() {
        DynamicForm df = form().bindFromRequest();
        String titel = df.get("tp-name");
        String user = session().get("email");
        List<Trainingsplan> t = Trainingsplan.findByUser(user);
        for (Trainingsplan el : t) {
            el.setTitle(titel);
            Ebean.update(el);
        }
        return redirect("/tpfertig");
    }




    public static Result addToCart(int artikelnummer) {


        if (session().get("email") == null) {
            flash("korbError", "Du musst eingeloggt sein um einzukaufen");
            return redirect("/shop");
        } else {

            String email = session().get("email");

            Ebean.save(new Warenkorb(email, artikelnummer));

        }
        return redirect("/shop");
    }
    public static int anzahl(){
        if (session().get("email") == null) {
           return 0;
        } else {

            String email = session().get("email");

           int anzahl = Warenkorb.find.where().eq("user", email).findRowCount();
            return anzahl;
        }

    }


    public static void speicherProdukt() {

        Ebean.save(new Produkt("Testosteron", (19.99f), 5, "Testosteron: Staerkt und Entwickelt ihre Muskulator, ein Zeichen fuer Maennlichkeit", "Muskelaufbau", "/assets/images/shop/produkte/testo.jpg"));
        Ebean.save(new Produkt("L-Carnitin", (9.99f), 10, "L-Carnitin sorgt fuer einen schneller Stoffwechsel", "Metabolisma", "/assets/images/shop/produkte/lcarnitin.png"));
        Ebean.save(new Produkt("Eiweiss", (29.99f), 25, "Eiweiss: Staerkt und Entwickelt ihre Muskulator, Duennschiss vorprogrammiert", "Ernaehrung", "/assets/images/shop/produkte/eishake.jpg"));
        Ebean.save(new Produkt("Creatin", (24.99f), 100, "Creatin Nahrungsergaenzung", "Ernaehrung", "/assets/images/shop/produkte/creatin.jpg"));
        Ebean.save(new Produkt("Shaker", (5.99f), 100, "Shaker", "Anderes", "/assets/images/shop/produkte/shaker.jpg"));
        Ebean.save(new Produkt("Proriegel", (2.99f), 100, "Protein-Riegel unterstuetzt Muskelaufbau", "Muskelaufbau", "/assets/images/shop/produkte/proriegel.jpg"));
        Ebean.save(new Produkt("Proshake", (159.99f), 100, "Protein-Shake unterstuetzt Muskelaufbau", "Muskelaufbau", "/assets/images/shop/produkte/proshake.jpg"));
        Ebean.save(new Produkt("Handschuhe", (14.99f), 100, "Cool-designed, ergnomisch, praktisch und hautfreundlich", "Anderes", "/assets/images/shop/produkte/handschuhe.jpg"));

    }


    //USEFULL SHIT

    public static Result reg() {
        return ok(register.render(user_form));
    }

    public static Result home() {return ok(home.render());
    }

    public static Result tp() {
        return ok(trainingsplan.render());
    }

    public static Result login() {
        return ok(login.render(user_form));
    }


    public static Result shop() {

        //speicherProdukt();

        List<Produkt> p = Produkt.find.findList();



        return ok(shop.render(p));
    }
    public static Result warenkorb() {

        if (session().get("email") == null) {
        flash("korbError", "Du musst eingeloggt sein um einzukaufen");
        return redirect("/shop");
    } else {

        String email = session().get("email");

        List<Warenkorb> w = Warenkorb.find.where().eq("user", email).findList();
        List<Produkt> p = new ArrayList<>();
        for (Warenkorb wa : w) {

            int artikelnummer = wa.artikelnummer;

            List<Produkt> p2 = Produkt.find.where().eq("artikelnummer", artikelnummer).findList();
            p.add(p2.get(0));


        }



        return ok(warenkorb.render(p));
                 }
    }
    public static Result deleteWarenkorb(){

        Ebean.delete(Warenkorb.find.findList());
        return warenkorb();

    }

    public static Result deleteProdukt(int artikelnummer){

        Ebean.delete(Warenkorb.find.where().eq("artikelnummer", artikelnummer).findList());

        return warenkorb();

    }

    public static float gesamtpreis(){
        String email = session().get("email");

        float gesamtpreis = 0f;
        float zwischenpreis;
        List<Warenkorb> w = Warenkorb.find.where().eq("user", email).findList();
        for (Warenkorb wa : w) {
            int artikelnummer = wa.artikelnummer;

            List<Produkt> p2 = Produkt.find.where().eq("artikelnummer", artikelnummer).findList();
             zwischenpreis = p2.get(0).preis;
            gesamtpreis += zwischenpreis;

        }
        return gesamtpreis;
    }
    public static Result main() {
        return ok(flash(Users.checkSession()));
    }

    public static Result logout() {
        session().clear();

        return redirect(
                routes.Application.home()
        );
    }


    //Login class...
    public static class Login {
        String email;
        String password;

    }
}



