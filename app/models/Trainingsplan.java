package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.ExpressionList;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.util.List;

@Entity
@SequenceGenerator(name="name")
public class Trainingsplan {


    @GeneratedValue
    @Id
    protected int id;

    @Constraints.Required
    public String user;

    public String titel;

    public String uebung;

    public String wiederholungen;

    public String saetze;

    public Trainingsplan(String user, String titel, String uebung,  String saetze, String wiederholungen) {

        this.user = user;
        this.titel = titel;
        this.uebung = uebung;
        this.wiederholungen = wiederholungen;
        this.saetze = saetze;
    }

public static Model.Finder<String, Trainingsplan> find = new Model.Finder(String.class, Trainingsplan.class);

    public static void deleteTrainingsplan(String user){
       Ebean.delete(find.where().eq("user", user).findList());
    }

    public static List<Trainingsplan> findByUser(String user){
    return find.where().eq("user", user).findList();
}
    public static void deleteUbung (String user, String ubung, String saetze, String wiederholungen){
        Ebean.delete(find.where().eq("user", user).eq("uebung",ubung).eq("saetze", saetze)
                .eq("wiederholungen", wiederholungen).findList());
    }

    public void setTitle(String title) {
        this.titel = title;
    }


    public String getTitel() {

        return titel;
    }

    @Override
    public String toString() {
        return "Trainingsplan{" +
                "uebung='" + uebung + '\'' +
                ", wiederholungen='" + wiederholungen + '\'' +
                ", saetze='" + saetze + '\'' +
                " titel" + titel+
                '}';
    }
}