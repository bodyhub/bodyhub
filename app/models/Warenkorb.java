package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.util.List;

@Entity
@SequenceGenerator(name="warenkorb")
public class Warenkorb extends Model{

    @GeneratedValue
    @Id
    protected int warenkorbid;

    @Constraints.Required
    public String user;

    public int artikelnummer;

    public Warenkorb(String user, int artikelnummer) {
        this.user = user;
        this.artikelnummer = artikelnummer;
    }

    public String getUser() {
        return user;
    }

    public int getArtikelnummer() {
        return artikelnummer;
    }

    public void setArtikelnummer(int artikelnummer) {
        this.artikelnummer = artikelnummer;
    }

    public void setUser(String user) {

        this.user = user;
    }

    public int getWarenkorbid() {
        return warenkorbid;
    }

    public void setWarenkorbid(int warenkorbid) {
        this.warenkorbid = warenkorbid;
    }




    public static Model.Finder<String, Warenkorb> find = new Model.Finder(String.class, Warenkorb.class);
}