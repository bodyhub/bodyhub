package models;

import java.util.*;
import javax.persistence.*;

import com.avaje.ebean.*;
import play.data.validation.*;
import play.db.ebean.*;

import static play.mvc.Controller.session;
import static play.mvc.Results.redirect;


@Entity
public class Users extends Model {
    @Id
    @Constraints.Required
    @Constraints.Email
    public String email;

    //@Constraints.Required
    @Column(unique = true)
    public String username;

    //@Constraints.Required
    public String firstName;

    //@Constraints.Required
    public String lastName;

    @Constraints.Required
    //@Constraints.MinLength(6)
    @Constraints.MaxLength(20)
    public String password;





    public static Model.Finder<String, Users> find = new Model.Finder(String.class, Users.class);

    public static Users findByEmail(String email) {
        return find.byId(email);
    }

    public static Users findByUsername(String username) {
        return find.where().eq("username", username).findUnique();
    }

    public static Users findUser(String email, String password) {
        return find.where().eq("email", email).eq("password", password).findUnique();
    }



    public static String checkSession() {
        String curentUser = session().get("email");
        if (curentUser != null) {
            if (findByEmail(curentUser) != null) {
                return curentUser;
            }
        } else {
            return null;
        }
        return null;
    }




}
