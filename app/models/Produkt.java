package models;


import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="produkt")
public class Produkt extends Model {

    @GeneratedValue
    @Id
    public int artikelnummer;

    public String name;

    public float preis;

    public int anzahl;

    public String beschreibung;

    public String kategorie;

    public String bildurl;

    public int getArtikelnummer() {
        return artikelnummer;
    }

    public void setArtikelnummer(int artikelnummer) {
        this.artikelnummer = artikelnummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPreis() {
        return preis;
    }

    public void setPreis(float preis) {
        this.preis = preis;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public String getKategorie() {
        return kategorie;
    }

    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }

    public String getBildurl() {
        return bildurl;
    }

    public void setBildurl(String bildurl) {
        this.bildurl = bildurl;
    }

    public Produkt(String name, float preis, int anzahl,String beschreibung,  String kategorie, String bildurl) {
        this.name = name;
        this.beschreibung = beschreibung;
        this.anzahl = anzahl;
        this.preis = preis;
        this.kategorie = kategorie;
        this.bildurl = bildurl;
    }
    public static Model.Finder<String, Produkt> find = new Model.Finder(String.class, Produkt.class);


}
