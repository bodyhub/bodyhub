name := """bodyhub"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)


scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
 jdbc,
 javaEbean,
 cache,
 ws,
 "mysql" % "mysql-connector-java" % "5.1.34",
 "org.json"%"org.json"%"chargebee-1.0",
 "com.google.code.gson" % "gson" % "2.2",
 "it.innove" % "play2-pdf" % "1.1.3"
)
